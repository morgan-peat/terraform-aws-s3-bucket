resource "aws_s3_bucket" "this" {
  count = var.module_enabled ? 1 : 0

  bucket        = var.name
  force_destroy = var.force_destroy
  tags          = var.tags
}
