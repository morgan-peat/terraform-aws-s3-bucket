# This block specifies the TFC workspace to use when executing
# terraform using the CLI. This block is ignored for VCS workflows.
#
# See:
# https://developer.hashicorp.com/terraform/cli/cloud/settings#the-cloud-block
# https://developer.hashicorp.com/terraform/cli/cloud
terraform {
  cloud {
    organization = "mp-demo-org"

    workspaces {
      name = "terraform-aws-s3-bucket"
    }
  }
}
