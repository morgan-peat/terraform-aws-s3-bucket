resource "random_pet" "bucket_name" {
  length = 4
}

module "bucket" {
  source = "../.."

  name = random_pet.bucket_name.id
}

module "disabled_bucket" {
  source = "../.."

  name           = "${random_pet.bucket_name.id}-disabled"
  module_enabled = false
}
