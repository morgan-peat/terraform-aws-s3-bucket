output "bucket_arn" {
  value = module.bucket.arn
}

output "disabled_bucket_arn" {
  value = module.disabled_bucket.arn
}
