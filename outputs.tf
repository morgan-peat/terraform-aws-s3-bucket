output "id" {
  description = "The name of the bucket."
  value       = one(aws_s3_bucket.this[*].id)
}

output "arn" {
  description = "The ARN of the bucket. Will be of format arn:aws:s3:::bucketname."
  value       = one(aws_s3_bucket.this[*].arn)
}

output "region" {
  description = "The AWS region this bucket resides in."
  value       = one(aws_s3_bucket.this[*].region)
}
